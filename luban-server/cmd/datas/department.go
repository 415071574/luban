package datas

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
	"github.com/gookit/color"
	"gorm.io/gorm"
	"os"
	"time"
)

func InitSysDepartment(db *gorm.DB) {
	status := new(bool)
	*status = true
	Depart := []model.SysDepartment{
		{GVA_MODEL: global.GVA_MODEL{ID: 1, CreatedAt: time.Now(), UpdatedAt: time.Now()},DeptCode: "100001",DeptName: "IT部",Status: status,Remark: "IT部",CompanyCode: "100001"},
	}
	if err := db.Transaction(func(tx *gorm.DB) error {
		if tx.Where("id IN ?", []int{1}).Find(&[]model.SysDepartment{}).RowsAffected == 1 {
			color.Danger.Println("sys_department表的初始数据已存在!")
			return nil
		}
		if err := tx.Create(&Depart).Error; err != nil { // 遇到错误时回滚事务
			return err
		}
		return nil
	}); err != nil {
		color.Warn.Printf("[Mysql]--> sys_department 表的初始数据失败,err: %v\n", err)
		os.Exit(0)
	}
}
