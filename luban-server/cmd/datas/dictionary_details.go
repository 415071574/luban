package datas

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
	"github.com/gookit/color"
	"gorm.io/gorm"
	"os"
	"time"
)

func InitSysDictionaryDetail(db *gorm.DB) {
	status := new(bool)
	*status = true
	DictionaryDetail := []model.SysDictionaryDetail{
		{GVA_MODEL: global.GVA_MODEL{ID: 1, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "sys_common_status",ItemName: "开启",ItemValue: "1",Remark: "开启状态",Sort: 1,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 2, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "sys_common_status",ItemName: "关闭",ItemValue: "0",Remark: "关闭状态",Sort: 2,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 3, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "sys_user_sex",ItemName: "男",ItemValue: "0",Remark: "男",Sort: 1,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 4, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "sys_user_sex",ItemName: "女",ItemValue: "1",Remark: "女",Sort: 2,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 5, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "programming_language",ItemName: "JAVA",ItemValue: "java",Remark: "JAVA",Sort: 1,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 6, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "programming_language",ItemName: "Golang",ItemValue: "go",Remark: "Golang",Sort: 2,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 7, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "programming_language",ItemName: "Python",ItemValue: "python",Remark: "Python",Sort: 3,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 8, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "programming_language",ItemName: "PHP",ItemValue: "php",Remark: "PHP",Sort: 4,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 9, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "project_attribute",ItemName: "自研",ItemValue: "development",Remark: "公司内部研发",Sort: 1,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 10, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "project_attribute",ItemName: "外采",ItemValue: "procurement",Remark: "外部采购",Sort: 2,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 11, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "env_name",ItemName: "生产",ItemValue: "prod",Remark: "生产环境",Sort: 1,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 12, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "env_name",ItemName: "预发",ItemValue: "uat",Remark: "预发环境",Sort: 2,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 13, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "env_name",ItemName: "测试",ItemValue: "test",Remark: "测试环境",Sort: 3,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 14, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "env_name",ItemName: "开发",ItemValue: "dev",Remark: "开发环境",Sort: 4,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 15, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "os_type",ItemName: "Windos",ItemValue: "windos",Remark: "Windos系统",Sort: 1,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 16, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "os_type",ItemName: "Linux",ItemValue: "linux",Remark: "Linux系统",Sort: 2,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 17, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "os_type",ItemName: "Android",ItemValue: "Android",Remark: "Android系统",Sort: 3,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 18, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "os_system",ItemName: "centos7",ItemValue: "centos7",Remark: "centos7",Sort: 1,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 19, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "os_system",ItemName: "ubuntu",ItemValue: "ubuntu",Remark: "ubuntu",Sort: 2,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 20, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "os_system",ItemName: "centos6",ItemValue: "centos6",Remark: "centos6",Sort: 3,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 21, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "os_system",ItemName: "windos-server12",ItemValue: "windos-server12",Remark: "windos-server12",Sort: 4,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 22, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "cpu_num",ItemName: "2核",ItemValue: "2C",Remark: "2核",Sort: 1,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 23, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "cpu_num",ItemName: "4核",ItemValue: "4C",Remark: "4核",Sort: 2,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 24, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "cpu_num",ItemName: "8核",ItemValue: "8C",Remark: "8核",Sort: 3,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 25, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "cpu_num",ItemName: "16核",ItemValue: "16C",Remark: "16核",Sort: 4,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 26, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "cpu_num",ItemName: "32核",ItemValue: "32C",Remark: "32核",Sort: 5,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 27, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "mem_info",ItemName: "4GB",ItemValue: "4GB",Remark: "4GB",Sort: 1,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 28, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "mem_info",ItemName: "8GB",ItemValue: "8GB",Remark: "8GB",Sort: 2,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 29, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "mem_info",ItemName: "12GB",ItemValue: "12GB",Remark: "12GB",Sort: 3,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 30, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "mem_info",ItemName: "16GB",ItemValue: "16GB",Remark: "16GB",Sort: 4,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 31, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "mem_info",ItemName: "32GB",ItemValue: "32GB",Remark: "32GB",Sort: 5,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 32, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "mem_info",ItemName: "64GB",ItemValue: "64GB",Remark: "64GB",Sort: 6,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 33, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "mem_info",ItemName: "128GB",ItemValue: "128GB",Remark: "128GB",Sort: 7,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 34, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "disk_space",ItemName: "50GB",ItemValue: "50GB",Remark: "50GB",Sort: 1,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 35, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "disk_space",ItemName: "100GB",ItemValue: "100GB",Remark: "100GB",Sort: 2,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 36, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "disk_space",ItemName: "200GB",ItemValue: "200GB",Remark: "200GB",Sort: 3,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 37, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "disk_space",ItemName: "300GB",ItemValue: "300GB",Remark: "300GB",Sort: 4,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 38, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "disk_space",ItemName: "500GB",ItemValue: "500GB",Remark: "500GB",Sort: 5,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 39, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "disk_space",ItemName: "1024GB",ItemValue: "1024GB",Remark: "1024GB",Sort: 6,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 40, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "disk_space",ItemName: "1536GB",ItemValue: "1536GB",Remark: "1536GB",Sort: 7,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 41, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "system_user",ItemName: "root",ItemValue: "root",Remark: "root",Sort: 1,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 42, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "system_user",ItemName: "admin",ItemValue: "admin",Remark: "admin",Sort: 2,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 43, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "system_user",ItemName: "devuser",ItemValue: "devuser",Remark: "devuser",Sort: 3,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 44, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "system_user",ItemName: "appuser",ItemValue: "appuser",Remark: "appuser",Sort: 4,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 45, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "server_idc",ItemName: "上汽云",ItemValue: "SAIC-ClOUD",Remark: "上汽云",Sort: 1,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 46, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "server_idc",ItemName: "尚凯9楼",ItemValue: "SHANG-KAI-9",Remark: "尚凯9楼",Sort: 2,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 47, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "server_idc",ItemName: "电信机房",ItemValue: "TELECOM",Remark: "电信机房",Sort: 3,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 48, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "server_idc",ItemName: "移动机房",ItemValue: "SHIFT",Remark: "移动机房",Sort: 4,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 49, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "database_tpye",ItemName: "Mysql",ItemValue: "mysql",Remark: "Mysql",Sort: 1,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 50, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "database_tpye",ItemName: "Oracle",ItemValue: "oracle",Remark: "Oracle",Sort: 2,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 51, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "database_tpye",ItemName: "MongoDB",ItemValue: "mongodb",Remark: "MongoDB",Sort: 3,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 52, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "database_tpye",ItemName: "SqlServer",ItemValue: "sqlServer",Remark: "SqlServer",Sort: 4,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 53, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "redis_mode",ItemName: "单机",ItemValue: "1",Remark: "单机",Sort: 1,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 54, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "database_tpye",ItemName: "主从",ItemValue: "2",Remark: "主从",Sort: 2,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 55, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "database_tpye",ItemName: "哨兵",ItemValue: "3",Remark: "哨兵",Sort: 3,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 56, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "database_operation_auth",ItemName: "读写",ItemValue: "WR",Remark: "读写权限",Sort: 1,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 57, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "database_operation_auth",ItemName: "只读",ItemValue: "R",Remark: "查询权限",Sort: 3,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 58, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "security_put",ItemName: "入方向",ItemValue: "input",Remark: "入方向",Sort: 1,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 59, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "security_put",ItemName: "出方向",ItemValue: "output",Remark: "出方向",Sort: 2,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 60, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "sec_protocol",ItemName: "http",ItemValue: "http",Remark: "http",Sort: 1,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 61, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "sec_protocol",ItemName: "https",ItemValue: "https",Remark: "https",Sort: 2,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 62, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "sec_protocol",ItemName: "tcp",ItemValue: "tcp",Remark: "tcp",Sort: 3,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 63, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "sec_protocol",ItemName: "udp",ItemValue: "udp",Remark: "udp",Sort: 4,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 64, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "apps_type",ItemName: "前端",ItemValue: "web",Remark: "前端应用",Sort: 1,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 65, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "apps_type",ItemName: "后端",ItemValue: "service",Remark: "后端应用",Sort: 2,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 66, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "apps_type",ItemName: "公用包",ItemValue: "common",Remark: "公用包",Sort: 3,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 67, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "apps_type",ItemName: "安卓包",ItemValue: "apk",Remark: "安卓包",Sort: 4,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 68, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "deploy_status",ItemName: "未创建",ItemValue: "0",Remark: "jenkins上未创建job",Sort: 1,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 69, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "deploy_status",ItemName: "已创建",ItemValue: "1",Remark: "jenkins上已创建job",Sort: 2,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 70, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "deploy_status",ItemName: "已更新",ItemValue: "2",Remark: "应用配置有更新，未同步到jenkin上去",Sort: 3,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 71, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "deploy_status",ItemName: "已同步",ItemValue: "3",Remark: "应用配置更新后，已同步到Jenkins上",Sort: 4,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 72, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "tool-usage",ItemName: "Gitlab",ItemValue: "gitlab",Remark: "Gitlab地址",Sort: 1,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 73, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "tool-usage",ItemName: "Jenkins",ItemValue: "jenkins",Remark: "Jenkins",Sort: 2,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 74, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "tool-usage",ItemName: "OS_ROOT",ItemValue: "os_root",Remark: "操作系统管理员",Sort: 3,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 75, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "tool-usage",ItemName: "OS_APPUSER",ItemValue: "os_appuser",Remark: "操作系统appuser用户",Sort: 4,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 76, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "tool-usage",ItemName: "OS_DEVUSER",ItemValue: "os_devuser",Remark: "操作系统devuser用户",Sort: 5,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 77, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "tool-usage",ItemName: "SAIC_PROXY",ItemValue: "saic_proxy",Remark: "上汽云代理",Sort: 6,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 78, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "tool-usage",ItemName: "AWS_PROXY",ItemValue: "aws_proxy",Remark: "AWS代理",Sort: 7,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 79, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "tool-usage",ItemName: "Ansible",ItemValue: "ansible",Remark: "Ansible",Sort: 8,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 80, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "build_status",ItemName: "运行中",ItemValue: "1",Remark: "运行中",Sort: 1,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 81, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "build_status",ItemName: "成功",ItemValue: "2",Remark: "成功",Sort: 2,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 82, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "build_status",ItemName: "失败",ItemValue: "3",Remark: "失败",Sort: 3,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 83, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "build_status",ItemName: "超时",ItemValue: "4",Remark: "超时--回调接口超时",Sort: 4,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 84, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "ssh_connect_proxy",ItemName: "上汽云",ItemValue: "saic_proxy",Remark: "上汽云",Sort: 1,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 85, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "ssh_connect_proxy",ItemName: "亚马逊",ItemValue: "aws_proxy",Remark: "亚马逊",Sort: 2,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 86, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "ssh_connect_proxy",ItemName: "本地",ItemValue: "connect",Remark: "本地",Sort: 3,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 87, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "ssh_connect_type",ItemName: "密码",ItemValue: "password",Remark: "密码连接",Sort: 1,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 88, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "ssh_connect_type",ItemName: "密钥",ItemValue: "key",Remark: "密钥连接",Sort: 2,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 89, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "ssh_audit_status",ItemName: "未标记",ItemValue: "1",Remark: "未标记状态",Sort: 1,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 90, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "ssh_audit_status",ItemName: "正常",ItemValue: "2",Remark: "正常状态",Sort: 2,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 91, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "ssh_audit_status",ItemName: "警告",ItemValue: "4",Remark: "警告状态",Sort: 3,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 92, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "ssh_audit_status",ItemName: "危险",ItemValue: "8",Remark: "危险状态",Sort: 4,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 93, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "ssh_audit_status",ItemName: "致命",ItemValue: "16",Remark: "致命状态",Sort: 5,Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 94, CreatedAt: time.Now(), UpdatedAt: time.Now()},DictCode: "ssh_audit_status",ItemName: "VIM",ItemValue: "32",Remark: "Editor状态",Sort: 6,Status: status},

	}
	if err := db.Transaction(func(tx *gorm.DB) error {
		if tx.Where("id IN ?", []int{1, 94}).Find(&[]model.SysDictionaryDetail{}).RowsAffected == 2 {
			color.Danger.Println("sys_dictionary_details表的初始数据已存在!")
			return nil
		}
		if err := tx.Create(&DictionaryDetail).Error; err != nil { // 遇到错误时回滚事务
			return err
		}
		return nil
	}); err != nil {
		color.Warn.Printf("[Mysql]--> sys_dictionary_details 表的初始数据失败,err: %v\n", err)
		os.Exit(0)
	}
}
