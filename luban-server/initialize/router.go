package initialize

import (
	_ "gin-luban-server/docs"
	"gin-luban-server/global"
	"gin-luban-server/middleware"
	"gin-luban-server/router"
	"github.com/gin-gonic/gin"
	"github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	"net/http"
)

// 初始化总路由

func Routers() *gin.Engine {
	var Router = gin.Default()
	Router.StaticFS(global.GVA_CONFIG.Local.Path, http.Dir(global.GVA_CONFIG.Local.Path)) // 为用户头像和文件提供静态地址
	// Router.Use(middleware.LoadTls())  // 打开就能玩https了
	global.GVA_LOG.Info("use middleware logger")
	// 跨域
	Router.Use(middleware.Cors())
	global.GVA_LOG.Info("use middleware cors")
	Router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	global.GVA_LOG.Info("register swagger handler")
	// 方便统一添加路由组前缀 多服务器上线使用
	PublicGroup := Router.Group("")
	{
		router.InitBaseRouter(PublicGroup) // 注册基础功能路由 不做鉴权
	}
	AnsibleGroup :=PublicGroup.Group("ansible")
	{
		router.InitAnsibleHostInitRouter(AnsibleGroup)
	}
	JumpPublic := PublicGroup.Group("jump")
	{
		router.InitJumpServerWS(JumpPublic)  //ws
		router.InitJumpServerSftp(JumpPublic) //sftp
	}
	PrivateGroup := Router.Group("")
	PrivateGroup.Use(middleware.JWTAuth()).Use(middleware.CasbinHandler())
	{
		router.InitApiRouter(PrivateGroup)                 // 注册功能api路由
		router.InitJwtRouter(PrivateGroup)                 // jwt相关路由
		router.InitUserRouter(PrivateGroup)                // 注册用户路由
		router.InitMenuRouter(PrivateGroup)                // 注册menu路由
		router.InitEmailRouter(PrivateGroup)               // 邮件相关路由
		router.InitSystemRouter(PrivateGroup)              // system相关路由
		router.InitCasbinRouter(PrivateGroup)              // 权限相关路由
		router.InitAuthorityRouter(PrivateGroup)           // 注册角色路由
		router.InitSysDictionaryRouter(PrivateGroup)       // 字典管理
		router.InitCompanyRouter(PrivateGroup)             // 公司管理
		router.InitBasicConfigureRouter(PrivateGroup)       // 基础配置
		router.InitDepartmentRouter(PrivateGroup)          // 部门管理
		router.InitStationRouter(PrivateGroup)             // 岗位管理
		router.InitSysOperationRecordRouter(PrivateGroup)  // 操作记录
		router.InitSysDictionaryDetailRouter(PrivateGroup) // 字典详情管理
		router.InitSysLoginLogsRouter(PrivateGroup)        //登陆日志
		router.InitCMDBProjectRouter(PrivateGroup)         // CMDB 项目管理
		// CMDB 相关路由
		CMDBPrivateGroup := PrivateGroup.Group("cmdb")
		{
			router.InitCMDBProjectRouter(CMDBPrivateGroup) // CMDB 项目管理路由
			router.InitCMDBDomainRouter(CMDBPrivateGroup) // CMDB 项目管理路由
			router.InitCMDBSERVER(CMDBPrivateGroup) //CMDBD的server
			router.InitCMDBSUCURITY(CMDBPrivateGroup) //CMDBD的security
			router.InitCMDBRedisRouter(CMDBPrivateGroup) //CMDBD的Redis信息
            router.InitCMDBDatabaseRouter(CMDBPrivateGroup)  //CMDBD 数据库
			router.InitCMDBDatabaseUserRouter(CMDBPrivateGroup) //CMDBD 数据库账号
		}
		DeployPrivateGroup := PrivateGroup.Group("deploy")
		{
			router.InitDeployAppRouter(DeployPrivateGroup)  //持续集成部署菜单树
			router.InitDeployAppConfigureRouter(DeployPrivateGroup) //持续集成应用配置
			router.InitDeployBuild(DeployPrivateGroup)  //持续集成编译
			router.InitDeployAppJenkinsRouter(DeployPrivateGroup) //jenkins接口
	    }
        JumpPrivateGroup := PrivateGroup.Group("jump")
        {
        	router.InitJumpServerUser(JumpPrivateGroup) //添加服务器
        	router.InitJumpServerFilter(JumpPrivateGroup)
        	router.InitJumpServerLogs(JumpPrivateGroup)

		}

		// NGINX 管理平台
		NginxPrivateGroup := PrivateGroup.Group("/nginx")
		{
			router.InitNginxRouter(NginxPrivateGroup)
		}

	}
	global.GVA_LOG.Info("router register success")
	return Router
}
