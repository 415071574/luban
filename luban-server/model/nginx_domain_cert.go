// 自动生成模板NginxDomainCert
package model

import (
	"gin-luban-server/global"
)

// 如果含有time.Time 请自行import time包
// 证书保存到数据库中
type NginxDomainCert struct {
	global.GVA_MODEL
	CertName  string `json:"cert_name" form:"cert_name" gorm:"uniqueIndex;column:cert_name;comment:"`
	Pem  string `json:"pem" form:"pem" gorm:"type:varchar(10240);comment:CA和公钥"`
	Key  string `json:"key" form:"key" gorm:"type:varchar(10240);comment:私钥"`
	Issued  string `json:"issued" form:"issued" gorm:"column:issued;comment:"`
	Dns  string `json:"dns" form:"dns" gorm:"column:dns;comment:"`
	Validity  string `json:"validity" form:"validity" gorm:"column:validity;comment:"`
	Deadline  string `json:"deadline" form:"deadline" gorm:"column:deadline;comment:"`
}


//func (NginxDomainCert) TableName() string {
//	return "nginx_domain_cert"
//}
