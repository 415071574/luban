package model

import "gin-luban-server/global"

type DeployBuildHistory struct {
	global.GVA_MODEL
	AppsName      	string   `json:"apps_name" form:"apps_name" gorm:"column:apps_name;comment:应用名称"`
	//PackageName      string   `json:"apps_code" form:"apps_code" gorm:"column:apps_code;comment:应用Code"`
	AppsJobName     string   `json:"apps_job_name" form:"apps_job_name" gorm:"column:apps_job_name;comment:应用jobName"`
	BranchName      string   `json:"branch_name" form:"branch_name" gorm:"column:branch_name;comment:分支名称"`
	TagName         string   `json:"tag_name" form:"tag_name" gorm:"column:tag_name;comment:标签名称"`
	TagInfo         string   `json:"tag_info" form:"tag_info" gorm:"column:tag_info;type:text;comment:标签相关信息"`
	Version     	string   `json:"version" form:"version" gorm:"column:version;comment:发布版本 时间戳或者生产TAG形式"`
	DeployEnv       string   `json:"deploy_env" form:"deploy_env" gorm:"column:deploy_env;comment:部署环境"`  // 字段后续可用于统计作用
	BuildNumber     int64      `json:"build_number" form:"build_number" gorm:"column:build_number;comment:构建的jenkinsID"`
	//BuildFile        string   `json:"build_file" form:"build_file" gorm:"column:build_file;comment:包文件名"`
	DeployStatus    string   `json:"deploy_status" form:"deploy_status" gorm:"column:deploy_status;comment:构建执行状态 1.正在部署 2. 部署成功 3. 部署失败"`
	DeployType     	string   `json:"deploy_type" form:"deploy_type" gorm:"column:deploy_type;default:normal;comment:发布类型 normal|rollback|restart"`
	BackupHost  string    `json:"backup_host" form:"backup_host" gorm:"column:backup_host;comment:远程备份包主机"`
	BackupPath    	string   `json:"backup_path" form:"backup_path" gorm:"column:backup_path;comment:远程备份包路径"`
	DeployUsername 	string	 `json:"deploy_username" form:"deploy_username" gorm:"column:deploy_username;comment:部署的用户"`
	DeployNickName string	  `json:"deploy_nick_name" form:"deploy_nick_name" gorm:"column:deploy_nick_name;comment:部署的用户的别名"`

	DeployAppConfigure DeployAppConfigure `json:"deploy_apply_configure" gorm:"foreignKey:AppsJobName;references:AppsJobName;comment:应用配置表"`
}

type VirtualCodeList struct {
	VirtualCode  string `json:"virtual_code"`
}

type ApplyDeployMap struct {
   Name string
}


