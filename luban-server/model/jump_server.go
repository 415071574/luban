package model

import (
	"gin-luban-server/global"
	"time"
)

type JsonArrayString []string
type JsonArraySshFilter []JumpServerCmdFilter

type JumpServerUser struct {
	global.GVA_MODEL
	UserName     string           `json:"user_name" form:"user_name" gorm:"column:user_name;type:varchar(128);not null;comment:用户名" `
	VirtualCode  string           `json:"virtual_code" form:"virtual_code" gorm:"column:virtual_code;type:varchar(255);not null;comment:服务器Code" `
	VirtualAlias string           `json:"virtual_alias" form:"virtual_alias" gorm:"column:virtual_alias;type:varchar(255);not null;comment:服务器名称别名" `
	SshIpAddress string           `json:"ssh_ip_address" form:"ssh_ip_address" gorm:"column:ssh_ip_address;type:varchar(128);not null;comment:ip地址" `
	ProjectCode  string           `json:"project_code" form:"project_code" gorm:"column:project_code;type:comment:关联标记;type:varchar(255)"`
	SshUser      string           `json:"ssh_user" form:"ssh_user" gorm:"column:ssh_user;type:varchar(128);not null;comment:用户名" `
	SshType      string           `json:"sshType" form:"sshType" gorm:"column:sshType;type:varchar(128);not null;comment:ssh密码类型" `
	SshKey       string           `json:"sshKey" form:"sshKey" gorm:"column:sshKey;type:text;comment:ssh密钥" `
	SshPassword  string           `json:"ssh_password" form:"ssh_password" gorm:"column:ssh_password;type:varchar(128);not null;comment:密码" `
	SshPort      string           `json:"ssh_port" form:"ssh_port" gorm:"column:ssh_port;type:varchar(128);not null;comment:端口" `
	XShellProxy  string           `json:"x_shell_proxy" form:"x_shell_proxy" gorm:"column:x_shell_proxy;comment:ssh代理;type:varchar(200);not null;size:200;"`
	ProxySshType string           `json:"proxy_sshType" form:"proxy_sshType" gorm:"column:proxy_sshType;comment:ssh代理密码类型;type:varchar(200);not null;size:200;"`
	ProxyUser    string           `json:"proxy_user" form:"proxy_user" gorm:"column:proxy_user;comment:ssh代理用户名;type:varchar(200);not null;size:200;"`
	ProxySshKey  string           `json:"proxy_sshKey" form:"proxy_sshKey" gorm:"column:proxy_sshKey;comment:ssh代理密钥;type:text"`
	ProxyPassword string          `json:"proxy_password" form:"proxy_password" gorm:"column:proxy_password;comment:ssh代理密码;type:varchar(200);not null;size:200;"`
	ProxyHost     string          `json:"proxy_host" form:"proxy_host" gorm:"column:proxy_host;comment:ssh代理主机IP;type:varchar(200);not null;size:200;"`
	ProxyPort     string          `json:"proxy_port" form:"proxy_port" gorm:"column:proxy_port;comment:ssh代理端口;type:varchar(200);not null;size:200;"`
	CMDBServers  []CmdbServer     `json:"CMDBServers" form:"CMDBServers" gorm:"foreignKey:VirtualCode;references:VirtualCode;comment:服务器Code" `
	Project      CMDBProject      `json:"project"  gorm:"foreignKey:ProjectCode;references:ProjectCode;comment:项目code"`
}

type  JumpServerCmdFilter struct {
	global.GVA_MODEL
	Command string `json:"command" form:"command" gorm:"column:command;type:varchar(128);comment:需要过滤的ssh命令 默认正则匹配" `
	Msg     string `json:"msg" form:"msg" gorm:"column:msg:command;type:varchar(128);comment:告警消息"`
	Enable  *bool   `json:"enable"  form:"enable" gorm:"column:enable;comment:1-开启 0-关闭"`
}

type SshFilterGroup struct {
	Filters []JumpServerCmdFilter `json:"filters" form:"filters"`
}


type JumpServerSshLogs struct {
	global.GVA_MODEL
	UserName     string           `json:"user_name" form:"user_name" gorm:"column:user_name;type:varchar(128);not null;comment:用户名" `
	SshUser      string           `json:"ssh_user" form:"ssh_user" gorm:"column:ssh_user;type:varchar(128);not null;comment:登录名" `
	ClientIp     string           `json:"client_ip" form:"client_ip" gorm:"column:client_ip;type:varchar(128);not null;comment:IP地址" `
	SshPort      string           `json:"ssh_port" form:"ssh_port" gorm:"column:ssh_port;type:varchar(128);not null;comment:端口" `
	Status       int              `json:"status" form:"status" gorm:"column:status;type:int(8);comment:1-未标记 2-正常 4-警告 8-危险 16-致命 32-VIM"`
	StartedAt    time.Time
	Remark       string           `json:"remark" form:"remark" gorm:"column:remark;type:varchar(128);not null;comment:备注" `
	WebClientLogs string          `json:"web_client_logs" form:"web_client_logs" gorm:"column:web_client_logs;type:text;not null;comment:客户端日志" `
}

type JumpServerTree struct {
	Id           uint       `json:"id"`        //id
	Label        string    `json:"label"`      //菜单名
	Children    []JumpServerTree  `json:"children" gorm:"-"`
}


