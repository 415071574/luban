package model

import "gin-luban-server/global"

//公司岗位表
type SysStation struct {
	global.GVA_MODEL
	StationCode string `json:"station_code" form:"station_code" gorm:"type:varchar(50);not null;comment:部门编码"`
	StationName string `json:"station_name" form:"station_name" gorm:"type:varchar(50);not null;comment:部门名称"`
	Remark      string `json:"remark" form:"remark" gorm:"type:varchar(100);not null;comment:描述" `
	Status      *bool  `json:"status" form:"status" gorm:"column:status;comment:状态"`
	CompanyCode string `json:"company_code" form:"company_code" gorm:"column:company_code;comment:关联标记"`
	DeptCode    string `json:"dept_code" form:"dept_code" gorm:"column:dept_code;comment:关联标记"`
	Company     SysCompany `json:"company" gorm:"foreignKey:CompanyCode;references:CompanyCode;comment:公司code"`
	Department  SysDepartment `json:"department" gorm:"foreignKey:DeptCode;references:DeptCode;comment:部门code"`
	SysUsers    []SysUser `json:"sysUsers" gorm:"foreignKey:StationCode;references:StationCode;comment:公司编码"`

}
