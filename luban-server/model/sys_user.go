package model

import (
	"gin-luban-server/global"
	"github.com/satori/go.uuid"
)

type SysUser struct {
	global.GVA_MODEL
	UUID        uuid.UUID    `json:"uuid" gorm:"comment:用户UUID"`
	Username    string       `json:"username" gorm:"comment:用户登录名"`
	Password    string       `json:"password" gorm:"comment:用户登录密码"`
	NickName    string       `json:"nick_name" gorm:"default:系统用户;comment:用户昵称" `
	Phone       string       `json:"phone" gorm:"size:11;not null;comment:手机号"`
	Sex         string       `json:"sex" gorm:"size:255;comment:性别" `
	Email       string       `json:"email" gorm:"type:varchar(150);not null;comment:邮箱" `
	Status      *bool        `json:"status"  gorm:"type:int(8);comment:状态"`
	DeptCode    string       `json:"dept_code" gorm:"type:int(8);comment:部门编码"`
	StationCode string       `json:"station_code" gorm:"type:int(8);comment:岗位编码"`
	CompanyCode string       `json:"company_code" gorm:"type:int(8);comment:岗位编码"`
	HeaderImg   string       `json:"headerImg" gorm:"default:http://qmplusimg.henrongyi.top/head.png;comment:用户头像"`
	Remark      string       `json:"remark" form:"remark" gorm:"comment:描述"`
	Authority   SysAuthority `json:"authority" gorm:"foreignKey:AuthorityId;references:AuthorityId;comment:用户角色"`
	AuthorityId string       `json:"authorityId" gorm:"default:admin;comment:用户角色Id"`
	DeployProd  bool         `json:"deploy_prod" gorm:"type:tinyint(1);not null;default:0;comment:是否有发布生产的权限"`

}
