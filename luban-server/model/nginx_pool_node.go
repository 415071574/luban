package model

import "gin-luban-server/global"

// 如果含有time.Time 请自行import time包
type NginxPoolNode struct {
	global.GVA_MODEL
	NodeName  string `json:"node_name" form:"node_name" gorm:"column:node_name;comment:;type:string;"`
	IP  string `json:"ip" form:"ip" gorm:"column:ip;comment:;type:string;"`
	Port  string `json:"port" form:"port" gorm:"column:port;comment:"`
	Weight  string `json:"weight" form:"weight" gorm:"column:weight;comment:"`
	MaxFailed  string `json:"max_failed" form:"max_failed" gorm:"column:max_failed;comment:"`
	TimeOut  string `json:"timeout" form:"timeout" gorm:"column:timeout;comment:"`
	Status  string `json:"status" form:"status" gorm:"column:status;comment:"`
	Cluster   NginxPool `json:"pool" gorm:"foreignKey:cluster_id;references:id"`
	ClusterID uint `json:"cluster_id" gorm:"column:cluster_id;not null;comment:集群ID"`
}

