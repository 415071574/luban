package response

import "gin-luban-server/model"

type JumpServerMenusResponse struct {
	Menus []model.JumpServerTree `json:"menus"`
}
