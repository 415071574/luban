package response

import "gin-luban-server/model"

type CMDBDomainResponse struct {
	Domain model.CMDBDomain `json:"domain"`
}
