package response

import "gin-luban-server/model"

type SysStationResponse struct {
	Station model.SysStation `json:"station"`
}
