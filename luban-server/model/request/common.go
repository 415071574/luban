package request

// Paging common input parameter structure
type PageInfo struct {
	Page     int `json:"page" form:"page" validate:"required"`
	PageSize int `json:"pageSize" form:"pageSize" validate:"required"`
}

// Find by id structure
type GetById struct {
	Id float64 `json:"id" form:"id" validate:"required"`
}

type IdsReq struct {
	Ids []int `json:"ids" form:"ids"`
}

// Get role by id structure
type GetAuthorityId struct {
	AuthorityId string  `json:"authorityId" form:"authorityId" validate:"required"`
}

type Empty struct {}

type TimeRange struct {
	// 时间戳格式
	StartTime string `json:"start_time,omitempty" form:"start_time"`
	EndTime string `json:"end_time,omitempty" form:"end_time"`
}