package request

import "gin-luban-server/model"

type GetCMDBProjectCode struct {
	CompanyCode string `json:"companyCode" validate:"required"`
}
// project分页条件查询及排序结构体
type SearchCMDBProjectParams struct {
	model.CMDBProject
	PageInfo
	Desc     bool   `json:"desc"`
}

type ProjectForm struct {
	model.CMDBProject
}


type ExcelProject struct {
	FileName string   `json:"fileName" form:"fileName"`
	model.CMDBProject
}