package request

import (
	"gin-luban-server/model"
)

type JumpServerLogsFrom struct {
	ID       int  `json:"id" form:"id" validate:"required"`
	Status   int  `json:"status" form:"status" validate:"required"`
}

type WebSshVirtualTermFrom struct {
	ID       float64  `json:"id"`
	Cols     int      `json:"cols"`
	Rows     int      `json:"rows"`
}

type JumpServerUserFrom struct {
	UserName     string           `json:"user_name" form:"user_name" validate:"required"`
	OsUser      string           `json:"os_user" form:"os_user" validate:"required`
	VirtualCodeInfos  []VirtualCodeInfo `json:"virtualCodeInfos"   validate:"required"`
}

type JumpServerCmdFilterFrom struct {
	Command string `json:"command" form:"command" validate:"required"`
	Msg     string `json:"msg" form:"msg" validate:"required"`
	Enable  *bool   `json:"enable"  form:"enable" validate:"required"`
}

type JumpServerParams struct {
	UserName     string    `json:"user_name" form:"user_name" validate:"required"`
}

type SearchJumpServerUserParams struct {
	model.JumpServerUser
	PageInfo
}

type SearchJumpServerCmdFilterParams struct {
	model.JumpServerCmdFilter
	PageInfo
}
type SearchJumpServerSshLogsParams struct {
	model.JumpServerSshLogs
	PageInfo
}

type UpdateJumpServerUserFrom struct {
	ID           float64          `json:"id" form:"id" validate:"required"`
	UserName     string           `json:"user_name" form:"user_name" validate:"required"`
	VirtualAlias string           `json:"virtual_alias" form:"virtual_alias" validate:"required"`
}