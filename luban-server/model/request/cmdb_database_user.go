package request

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
)

type SearchDatabaseUserParams struct {
	model.CMDBDatabaseUser
	PageInfo
}

type DatabaseUserForm struct {
	global.GVA_MODEL
	ClusterName        string `json:"cluster_name" form:"cluster_name" gorm:"size:255;column:cluster_name;comment:集群名称" validate:"required"`
	DatabaseName       string `json:"database_name" form:"database_name"  gorm:"comment:数据库实例" validate:"required"`
	UserName           string `json:"user_name" form:"user_name"  gorm:"comment:用户名称" validate:"required"`
	Password           string `json:"password" form:"password" gorm:"comment:密码" validate:"required" `
	Status             *bool  `json:"status" form:"status" gorm:"column:status;comment:状态"`
	Permission         string `json:"permission" form:"permission" gorm:"comment:权限" validate:"required"`
	ProjectCode        string `json:"project_code" form:"project_code" validate:"required"`

}

