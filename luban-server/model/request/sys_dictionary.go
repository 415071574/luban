package request

import "gin-luban-server/model"

type SysDictionarySearch struct{
    model.SysDictionary
    PageInfo
}

type  DictionaryForm struct {
    DictCode   string   `json:"dict_code" form:"dict_code" validate:"required" label:"字典编码"`
    DictName   string   `json:"dict_name" form:"dict_name"  validate:"required" label:"字典名"`
    DictDesc   string   `json:"dict_desc" form:"dict_desc"  validate:"required" label:"字典备注"`
    Status     *bool    `json:"status" form:"status" validate:"required" label:"状态"`
}

type DictCode struct {
    DictCode   string   `json:"dict_code" form:"dict_code" validate:"required" label:"字典编码"`
}