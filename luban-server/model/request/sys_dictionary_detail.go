package request

import "gin-luban-server/model"

type SysDictionaryDetailSearch struct{
    model.SysDictionaryDetail
    PageInfo
}

type DictionaryDetailForm struct {
    ItemName  string `json:"item_name" form:"item_name"  validate:"required" label:"展示值"`
    ItemValue string `json:"item_value" form:"item_value"  validate:"required" label:"字典值"`
    Sort      int    `json:"sort" form:"sort"validate:"required" label:"排序"`
    Status    *bool  `json:"status" form:"status"  validate:"required" label:"启用状态"`
    Remark    string `json:"remark" form:"remark" gorm:"comment:描述" `
    DictCode  string `json:"dict_code" form:"dict_code"  label:"字典编码" validate:"required" label:"字典值"`
}
