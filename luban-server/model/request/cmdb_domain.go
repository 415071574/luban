package request

import "gin-luban-server/model"


// project分页条件查询及排序结构体
type SearchCMDBDomainParams struct {
	model.CMDBDomain
	PageInfo
	Desc     bool   `json:"desc"`
}


type ExcelDomain struct {
	FileName string   `json:"fileName" form:"fileName"`
	model.CMDBDomain
}