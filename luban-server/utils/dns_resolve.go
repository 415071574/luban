package utils

import (
	"github.com/miekg/dns"
	"time"
)

// CNAME2 返回所有层的A记录地址
// src: 域名
// dnsService: dns服务器, 如114.114.114.114
func GetResolveIP(src string, dnsService string) (dst []string, err error) {
	c := dns.Client{
		Timeout: 5 * time.Second,
	}

	var lastErr error
	// retry 3 times
	for i := 0; i < 3; i++ {
		m := dns.Msg{}
		// 最终都会指向一个ip 也就是typeA, 这样就可以返回所有层的cname.
		m.SetQuestion(src+".", dns.TypeA)
		r, _, err := c.Exchange(&m, dnsService+":53")
		if err != nil {
			lastErr = err
			time.Sleep(1 * time.Second * time.Duration(i+1))
			continue
		}
		dst = []string{}
		for _, ans := range r.Answer {
			record, isType := ans.(*dns.A)
			if isType {
				dst = append(dst, record.A.String())
			}
		}
		lastErr = nil
		break
	}
	err = lastErr
	return
}