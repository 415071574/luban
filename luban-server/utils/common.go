package utils

import (
	"fmt"
	"math/rand"
)

func BoolToString(b bool) string {
	if b {
		return "正常"
	} else {
		return "关闭"
	}
}

//生成随机字符串
var Letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
func RandSeq(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = Letters[rand.Intn(len(Letters))]
	}
	return string(b)
}


// 截断字符串
func SubstrByByte(str string, length int) string {
	bs := []byte(str)
	byteLength := len([]byte(str))
	if byteLength <= length{
		return str
	} else {
		return "\nSkipping " + fmt.Sprintf("%d", byteLength-length) +"字符...\n" + string(bs[byteLength-length:])
	}

}