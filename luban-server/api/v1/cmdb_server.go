package v1

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
	"gin-luban-server/model/response"
	"gin-luban-server/service"
	"gin-luban-server/utils"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"strings"
)

// @Tags CmdbServer
// @Summary 修改mdbServer
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.CmdbServer true "修改CmdbServer"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/server/updateServer  [PUT]
func UpdateServer(c *gin.Context) {
	var cmdbServers model.CmdbServer
	_ = c.ShouldBindJSON(&cmdbServers)
	if err := service.UpdateCmdbServers(cmdbServers); err != nil {
		global.GVA_LOG.Error("更新失败!", zap.Any("err", err))
		response.FailWithMessage("更新失败!" + err.Error(), c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}

// @Tags CmdbServer
// @Summary 修改mdbServer
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.CmdbServersUpdateByIdAndStatusFrom true "修改CmdbServer"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/server/updateServer  [PUT]
func UpdateServerByIdAndStatus(c *gin.Context) {
	var cmdbServers request.CmdbServersUpdateByIdAndStatusFrom
	_ = c.ShouldBindJSON(&cmdbServers)
	if code,msg := utils.Validate(&cmdbServers);code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err := service.UpdateCmdbServersByIdAndStatus(cmdbServers.ID,cmdbServers.Status); err != nil {
		global.GVA_LOG.Error("更新失败!", zap.Any("err", err))
		response.FailWithMessage("更新失败", c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}

// @Tags CmdbServer
// @Summary 基于id查找CmdbServer
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.CmdbServer true "基于id查找CmdbServer"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/server/findServers  [POST]
func GetServerById(c *gin.Context){
		var cmdbServers model.CmdbServer
		_ = c.ShouldBind(&cmdbServers)
		if err, recmdbServers := service.GetCmdbServer(cmdbServers.ID); err != nil {
			global.GVA_LOG.Error("查询失败!", zap.Any("err", err))
			response.FailWithMessage("查询失败", c)
		} else {
			response.OkWithData(gin.H{"recmdbServers": recmdbServers}, c)
		}

}

// @Tags CmdbServer
// @Summary 删除CmdbServer
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.CmdbServer true "删除CmdbServer"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/server/deleteServer  [DELETE]

 func DeleteServer(c *gin.Context){
	 var cmdbServers model.CmdbServer
	 _ = c.ShouldBindJSON(&cmdbServers)
	 if err := service.DeleteCmdbServer(cmdbServers); err != nil {
		 global.GVA_LOG.Error("删除失败!", zap.Any("err", err))
		 response.FailWithMessage("删除失败", c)
	 } else {
		 response.OkWithMessage("删除成功", c)
	 }
 }


// @Tags CmdbServer
// @Summary 获取CmdbServer
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.CmdbServer true "获取CmdbServer"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/server/getServer  [GET]
// 获取CMDB项目
func GetServersByProjectsList(c *gin.Context) {
	var pageInfo request.CmdbServersByProjectsSearch
	_ = c.ShouldBind(&pageInfo)
	if err := utils.Verify(pageInfo.PageInfo, utils.PageInfoVerify); err != nil {
		response.FailValidateMessage(err.Error(), c)
		return
	}
	if err, list, total := service.GetCmdbServersByProjectsList(pageInfo); err != nil {
		global.GVA_LOG.Error("获取失败", zap.Any("err", err))
		response.FailWithMessage("获取失败", c)
	} else {
		response.OkWithDetailed(response.PageResult{
			List:     list,
			Total:    total,
			Page:     pageInfo.Page,
			PageSize: pageInfo.PageSize,
		}, "获取成功", c)
	}
}

// @Tags CmdbServer
// @Summary 创建CmdbServer
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.CmdbServerFrom true "创建CmdbServer"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/server/createServer  [POST]

func CreateServer(c *gin.Context) {
	var server request.CmdbServerFrom
	_ = c.ShouldBind(&server)
	if code,msg := utils.Validate(&server);code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	cmdbServers :=model.CmdbServer{VirtualName: server.VirtualName,VirtualCode: server.VirtualCode,ServerIdc: server.ServerIdc,Ipaddress: server.Ipaddress,
		User: server.User,Password: server.Password,Port: server.Port,Sshkey: server.Sshkey,OsSystem: server.OsSystem,OsType: server.OsType,
	EnvName: server.EnvName,CpuNum: server.CpuNum,MemInfo: server.MemInfo,ProjectCode: server.ProjectCode,CompanyCode: server.CompanyCode,DiskSpace: server.DiskSpace,XShellProxy: server.XShellProxy}
	if err := service.CreateCmdbServers(cmdbServers); err != nil {
			 global.GVA_LOG.Error("创建失败!", zap.Any("err", err))
			 response.FailWithMessage("创建失败!" + err.Error(), c)
		 } else {
		response.OkWithMessage("创建成功", c)
	}
}

// @Tags CmdbServer
// @Summary 获取CmdbServer
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.CmdbServersSearch true "获取CmdbServer"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/server/getServer  [GET]
// 获取CMDB项目
func GetServerList(c *gin.Context) {
		var pageInfo request.CmdbServersSearch
	     _ = c.ShouldBind(&pageInfo)
	    if err := utils.Verify(pageInfo.PageInfo, utils.PageInfoVerify); err != nil {
		response.FailValidateMessage(err.Error(), c)
		return
	    }
		if err, list, total := service.GetCmdbServersInfoList(pageInfo.CmdbServer, pageInfo.PageInfo, pageInfo.Desc); err != nil {
			global.GVA_LOG.Error("获取失败", zap.Any("err", err))
			response.FailWithMessage("获取失败", c)
		} else {
			response.OkWithDetailed(response.PageResult{
				List:     list,
				Total:    total,
				Page:     pageInfo.Page,
				PageSize: pageInfo.PageSize,
			}, "获取成功", c)
		}
}

// @Tags excel
// @Summary 导出Excel
// @Security ApiKeyAuth
// @accept application/json
// @Produce  application/octet-stream
// @Param data body request.ExcelServer true "导出Excel文件信息"
// @Success 200
// @Router /excel/exportExcel [get]
func ServerExportExcel(c *gin.Context) {
	var R request.ExcelServer
	// 绑定查询参数
	_ = c.ShouldBindQuery(&R)
	// 自动追加excel后缀名
	if ! strings.HasSuffix(R.FileName, ".xlsx") {
		R.FileName = R.FileName + ".xlsx"
	}
	filePath := global.GVA_CONFIG.Excel.Dir + R.FileName
	err := service.ParsesServerInfoList2Excel(R.CmdbServer, filePath)
	if err != nil {
		global.GVA_LOG.Error("转换Excel失败!", zap.Any("err", err))
		response.FailWithMessage("转换Excel失败", c)
		return
	}
	c.Writer.Header().Add("success", "true")
	c.File(filePath)
}