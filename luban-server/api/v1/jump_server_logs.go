package v1

import (
	"gin-luban-server/global"
	"gin-luban-server/model/request"
	"gin-luban-server/model/response"
	"gin-luban-server/service"
	"gin-luban-server/utils"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// @Tags JumpServerSshLogs
// @msp 删除堡垒机审计日志
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.JumpServerSshLogs true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"创建成功"}"
// @Router /jump/logs/deleteJumpServerSshLogs [delete]

func DeleteJumpServerSshLogs(c *gin.Context) {
	var Ids request.GetById
	_ = c.ShouldBindJSON(&Ids)
	if code,msg := utils.Validate(&Ids );code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err := service.DeleteJumpServerSshLogs(Ids.Id); err != nil {
		global.GVA_LOG.Error("删除失败!", zap.Any("err", err))
		response.FailWithMessage("删除失败!", c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}

// @Tags JumpServerSshLogs
// @Summary 批量删除JumpServerSshLogs
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除SysOperationRecord"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"批量删除成功"}"
// @Router /jump/logs/deleteJumpServerSshLogsByIds [delete]
func DeleteJumpServerSshLogsByIds(c *gin.Context) {
	var IDS request.IdsReq
	_ = c.ShouldBindJSON(&IDS)
	if err := service.DeleteJumpServerSshLogsByIds(IDS); err != nil {
		global.GVA_LOG.Error("批量删除失败!", zap.Any("err", err))
		response.FailWithMessage("批量删除失败", c)
	} else {
		response.OkWithMessage("批量删除成功", c)
	}
}

// @Tags JumpServerSshLogs
// @Summary 查找JumpServerSshLogsList
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.SearchJumpServerSshLogsParams true "页码, 每页大小, 搜索条件"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router  /jump/logs/getJumpServerSshLogsList [get]
func GetJumpServerSshLogsList(c *gin.Context) {
	var pageInfo request.SearchJumpServerSshLogsParams
	_ = c.ShouldBind(&pageInfo)
	if code,msg := utils.Validate(&pageInfo );code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err, list, total := service.GetJumpServerSshLogsInfoList(pageInfo); err != nil {
		global.GVA_LOG.Error("获取失败!", zap.Any("err", err))
		response.FailWithMessage("获取失败", c)
	} else {
		response.OkWithDetailed(response.PageResult{
			List:     list,
			Total:    total,
			Page:     pageInfo.Page,
			PageSize: pageInfo.PageSize,
		}, "获取成功", c)
	}
}

// @Tags JumpServerCmdFilter
// @msp 更新项目应用
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.JumpServerCmdFilter true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"创建成功"}"
// @Router /jump/filter/updateJumpServerCmdFilter [put]

func UpdateJumpServerSshLogs(c *gin.Context) {
	var logs request.JumpServerLogsFrom
	_ = c.ShouldBind(&logs)
	if code,msg := utils.Validate(&logs );code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err := service.UpdateJumpServerSshLogs(logs.ID,logs.Status); err != nil {
		global.GVA_LOG.Error("更新失败!", zap.Any("err", err))
		response.FailWithMessage("更新失败", c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}

