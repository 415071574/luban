package v1

import (
	"fmt"
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
	"gin-luban-server/model/response"
	"gin-luban-server/service"
	"gin-luban-server/utils"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"net/http"
	"strings"
)


// @Tags RedisCluster
// @msp 创建redis集群
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.RedisCluster true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"创建成功"}"
// @Router /cmdb/redis/createCluster [post]
func AddRedisCluster(c *gin.Context) {
	var redisCluster model.RedisCluster
	_ = c.ShouldBindJSON(&redisCluster)
	if err := service.AddRedisCluster(redisCluster); err != nil {
		response.FailWithMessage("创建失败" + err.Error(), c)
	} else {
		response.OkWithMessage("创建成功", c)
	}
}

// @Tags RedisCluster
// @Summary 删除redis集群
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.GetById true "根据ID删除"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /cmdb/redis/deleteCluster [delete]
func DeleteRedisCluster(c *gin.Context) {
	var redisCluster request.GetById
	_ = c.ShouldBindJSON(&redisCluster)
	if err := service.DeleteRedisCluster(redisCluster.Id); err != nil {
		response.FailWithMessage("创建失败", c)

	} else {
		response.OkWithMessage("创建成功", c)
	}
}

// @Tags RedisCluster
// @Summary 更新redis集群
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.RedisCluster true "更新redis集群"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /cmdb/redis/updateCluster [put]
func UpdateRedisCluster(c *gin.Context) {
	var R model.RedisCluster
	_ = c.ShouldBindJSON(&R)

	if err := service.UpdateRedisCluster(R); err != nil {
		global.GVA_LOG.Error("创建失败!", zap.Any("err", err))
		response.FailWithMessage("更新失败!" + err.Error(), c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}

// @Tags RedisCluster
// @Summary 通过ID查找redis集群
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.GetById true "根据id获取redis集群"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/redis/getClusterById [post]
func GetRedisClusterById(c *gin.Context) {
	var reqId request.GetById
	_ = c.ShouldBindJSON(&reqId)

	if err, results := service.GetRedisClusterById(reqId.Id); err != nil {
		global.GVA_LOG.Error("创建失败!", zap.Any("err", err))
		response.FailWithMessage("获取失败" + err.Error(), c)

	} else {
		response.OkWithDetailed(map[string]interface{} {
			"redis": results,
		}, "获取成功", c)

	}
}

// @Tags RedisCluster
// @Summary 查找集群列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.RedisCluster true "页码, 每页大小, 搜索条件"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/redis/getClusterList [post]
func GetRedisClusterList(c *gin.Context) {
	var pageInfo request.SearchRedisClusterParams
	_ = c.ShouldBind(&pageInfo)

	if err, list, total := service.GetRedisClusterList(pageInfo.RedisCluster, pageInfo.PageInfo, pageInfo.Desc); err != nil {
		response.FailWithMessage("获取失败" + err.Error(), c)

	} else {
		c.JSON(http.StatusOK, gin.H{
			"code": 0,
			"msg": "获取成功",
			"data": map[string]interface{} {
				"list": list,
				"total": total,
				"page": pageInfo.Page,
				"pageSize": pageInfo.PageSize,
			},
		})
	}
}

// @Tags excel
// @Summary 导出Excel
// @Security ApiKeyAuth
// @accept application/json
// @Produce  application/octet-stream
// @Param data body request.ExcelRedis true "导出Excel文件信息"
// @Success 200
// @Router /cmdb/redis/exportExcel [get]
func RedisExportExcel(c *gin.Context) {
	var R request.ExcelRedis
	// 绑定查询参数
	_ = c.ShouldBindQuery(&R)
	// 自动追加excel后缀名
	if ! strings.HasSuffix(R.FileName, ".xlsx") {
		R.FileName = R.FileName + ".xlsx"
	}
	filePath := global.GVA_CONFIG.Excel.Dir + R.FileName
	err := service.ParseRedisInfoList2Excel(R.RedisCluster, filePath)
	if err != nil {
		global.GVA_LOG.Error("转换Excel失败!", zap.Any("err", err))
		response.FailWithMessage("转换Excel失败", c)
		return
	}
	c.Writer.Header().Add("success", "true")
	c.File(filePath)
}




// @Tags RedisCluster
// @Summary 查询key
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.KeyScanParams true "集群ID, 数量, 库号, key"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/redis/key/query [post]
func KeyQuery(c *gin.Context)  {
	var queryInfo model.KeyScanParams
	_ = c.ShouldBindJSON(&queryInfo)
	fmt.Println(queryInfo)  //

	// 查询数据库
	keyInfo ,err := service.KeyQuery(queryInfo)
	if err != nil {
		global.GVA_LOG.Error("创建失败!", zap.Any("err", err))
		response.FailWithMessage( "" ,c)
	}
	response.OkWithDetailed(keyInfo, "获取成功", c)
}


// @Tags RedisCluster
// @Summary scan key
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.KeyScanParams true "集群ID, 数量, 库号, key"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/redis/key/scan [post]
func KeyScan(c *gin.Context)  {
	var queryInfo model.KeyScanParams
	_ = c.ShouldBindJSON(&queryInfo)
	fmt.Println(queryInfo)  //

	// 查询数据库
	list, err := service.KeyScan(queryInfo)
	if err != nil {
		global.GVA_LOG.Error("scan失败!", zap.Any("err", err))
		response.FailWithMessage("获取key失败" + err.Error(), c)
		return
	}
	response.OkWithDetailed(list, "获取成功", c)

}

// @Tags RedisCluster
// @Summary 获取redis库列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.GetRedisDbListParams "集群ID"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/redis/data/getDBList [post]

func GetDBList (c *gin.Context) {
	var queryInfo model.GetRedisDbListParams
	_ = c.ShouldBindJSON(&queryInfo)
	if list, err := service.GetDBList(queryInfo.ClusterID); err != nil {
		response.FailWithMessage("获取失败" + err.Error(), c)
	} else {
		response.OkWithDetailed(list, "获取成功", c)
	}
}
// @Tags RedisCluster
// @Summary 查询redis监控
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.RedisMonitorQueryParams true "集群ID, 数量, 库号, key"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/redis/monitor/getInfoItemMonitorData [post]
func GetInfoItemMonitorData(c *gin.Context)  {
	var queryInfo model.RedisMonitorQueryParams
	_ = c.ShouldBindJSON(&queryInfo)
	fmt.Println(queryInfo)  //

	// 查询数据库
	err, list := service.GetInfoItemMonitorData(queryInfo)
	if err != nil {
		global.GVA_LOG.Error("数据库查询失败!", zap.Any("err", err))
	}
	response.OkWithDetailed(list, "获取成功", c)

}

// @Tags RedisCluster
// @Summary 发送命令
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.GETRedisCommand true "集群ID, 数量, 库号, key"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/redis/command/sendCommand [post]
func SendCommand(c *gin.Context) {
	var getRedisCommand model.GETRedisCommand
	_ = c.ShouldBindJSON(&getRedisCommand)

	if err, results := service.SendCommand(getRedisCommand); err != nil {
		global.GVA_LOG.Error("命令发送失败!", zap.Any("err", err))
		response.FailWithMessage("失败" + err.Error(), c)
	} else {
		response.OkWithDetailed(results, "获取成功", c)
	}
}

// @Tags CMDBRedisDatabase
// @msp 创建Redis库记录
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.RedisDatabaseRecord true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"创建成功"}"
// @Router /cmdb/redis/createRedisDatabase [post]
func CreateRedisDatabase(c *gin.Context) {
	var R model.RedisDatabaseRecord
	_ = c.ShouldBindJSON(&R)
	if err := utils.Verify(R, utils.RedisDatabaseVerify); err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}

	if err := service.	CreateRedisDatabase(R); err != nil {
		global.GVA_LOG.Error("创建失败!", zap.Any("err", err))
		response.FailWithMessage("创建失败" + err.Error(), c)
	} else {
		response.OkWithMessage("创建成功", c)
	}
}


// @Tags CMDBRedisDatabase
// @Summary RedisDatabase
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.GetById true "根据ID删除"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /cmdb/redis/deleteRedisDatabase [delete]
func DeleteRedisDatabase(c *gin.Context) {
	var Redis request.GetById
	_ = c.ShouldBindJSON(&Redis)
	if code,msg := utils.Validate(&Redis );code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err := service.DeleteRedisDatabase(Redis.Id); err != nil {
		global.GVA_LOG.Error("删除失败!", zap.Any("err", err))
		response.FailWithMessage("删除失败", c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}

// @Tags CMDBRedisDatabase
// @Summary 更新Redis
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.RedisDatabaseRecord true "更新"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /cmdb/redis/updateRedis [put]
func UpdateRedisDatabase(c *gin.Context) {
	var R model.RedisDatabaseRecord
	_ = c.ShouldBindJSON(&R)
	if err := utils.Verify(R, utils.RedisVerify); err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := service.UpdateRedisDatabase(R); err != nil {
		global.GVA_LOG.Error("更新失败!", zap.Any("err", err))
		response.FailWithMessage("更新失败" + err.Error(), c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}



// @Tags CMDBRedisDatabase
// @Summary 查找RedisList
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.SearchCMDBRedisDatabaseParams true "页码, 每页大小, 搜索条件"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/redis/getRedisDatabaseList [get]
func GetRedisDatabaseList(c *gin.Context) {
	var pageInfo request.SearchCMDBRedisDatabaseParams
	_ = c.ShouldBindJSON(&pageInfo)
	if err := utils.Verify(pageInfo.PageInfo, utils.PageInfoVerify); err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err, list, total := service.GetRedisDatabaseInfoList(pageInfo); err != nil {
		global.GVA_LOG.Error("获取失败!", zap.Any("err", err))
		response.FailWithMessage("获取失败", c)
	} else {
		response.OkWithDetailed(response.PageResult{
			List:     list,
			Total:    total,
			Page:     pageInfo.Page,
			PageSize: pageInfo.PageSize,
		}, "获取成功", c)
	}
}


