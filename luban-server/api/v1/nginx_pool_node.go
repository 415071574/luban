package v1

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
	"gin-luban-server/model/response"
	"gin-luban-server/service"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// @Tags PoolNode
// @Summary 创建PoolNode
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.PoolNode true "创建PoolNode"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /nginx/PNode/createNginxPNode [post]
func CreateNginxPNode(c *gin.Context) {
	var PNode model.NginxPoolNode
	_ = c.ShouldBindJSON(&PNode)
	if err := service.CreateNginxPNode(PNode); err != nil {
		global.GVA_LOG.Error("创建失败!", zap.Any("err", err))
		response.FailWithMessage("创建失败", c)
	} else {
		response.OkWithMessage("创建成功", c)
	}
}



// @Tags NginxPool
// @Summary 删除NginxPool
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.NginxPool true "删除NginxPool"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /nginx/PNode/deleteNginxPNode [delete]
func DeleteNginxPNode(c *gin.Context) {
	var reqId request.GetById
	_ = c.ShouldBindJSON(&reqId)
	if err := service.DeleteNginxPNode(reqId.Id); err != nil {
		global.GVA_LOG.Error("删除失败", zap.Any("error", err.Error()))
		response.FailWithMessage("删除失败" + err.Error(), c)

	} else {
		response.OkWithMessage("删除成功", c)
	}
}


// @Tags PoolNode
// @Summary 更新PoolNode
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.PoolNode true "更新PoolNode"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /PNode/updatePoolNode [put]
func UpdateNginxPNode(c *gin.Context) {
	var PNode model.NginxPoolNode
	_ = c.ShouldBindJSON(&PNode)
	if err := service.UpdateNginxPNode(&PNode); err != nil {
		global.GVA_LOG.Error("更新失败!", zap.Any("err", err))
		response.FailWithMessage("更新失败", c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}



// @Tags NginxPool
// @Summary 用id查询NginxPool
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.NginxPoolNode true "用id查询NginxPool"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /nginx/PNode/GetNginxPNodeById [post]
func GetNginxPNodeById(c *gin.Context) {
	var reqId request.GetById
	_ = c.ShouldBind(&reqId)
	if err, poolNode := service.GetNginxPNodeById(reqId.Id); err != nil {
		global.GVA_LOG.Error("查询失败!", zap.Any("err", err))
		response.FailWithMessage("查询失败", c)
	} else {
		response.OkWithData(gin.H{"node": poolNode}, c)
	}
}


// @Tags PoolNode
// @Summary 分页获取PoolNode列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.NginxPoolNodeSearch true "分页获取PoolNode列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /nginx/PNode/getPoolNodeList [post]
func GetNginxPNodeList(c *gin.Context) {
	var pageInfo request.NginxPoolNodeSearch
	_ = c.ShouldBind(&pageInfo)
	if err, list, total := service.GetNginxPNodeList(pageInfo); err != nil {
		global.GVA_LOG.Error("获取失败", zap.Any("err", err))
		response.FailWithMessage("获取失败", c)
	} else {
		response.OkWithDetailed(response.PageResult{
			List:     list,
			Total:    total,
			Page:     pageInfo.Page,
			PageSize: pageInfo.PageSize,
		}, "获取成功", c)
	}
}
