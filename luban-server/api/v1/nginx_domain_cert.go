package v1

import (
	"fmt"
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
	"gin-luban-server/model/response"
	"gin-luban-server/service"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// @Tags NginxDomainCert
// @Summary 创建NginxDomainCert
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.NginxDomainCert true "创建NginxDomainCert"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /nginx/Cert/createNginxDomainCert [post]
func CreateNginxDomainCert(c *gin.Context) {
	var Cert model.NginxDomainCert
	_ = c.ShouldBindJSON(&Cert)
	fmt.Println(Cert.Pem, "------------")
	if err := service.CreateNginxDomainCertByFile(Cert); err != nil {
		global.GVA_LOG.Error("创建失败!", zap.Any("err", err))
		response.FailWithMessage("创建失败" + err.Error(), c)
	} else {
		response.OkWithMessage("创建成功", c)
	}
}

// @Tags NginxDomainCert
// @Summary 删除NginxDomainCert
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.NginxDomainCert true "删除NginxDomainCert"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /nginx/Cert/deleteNginxDomainCert [delete]

func DeleteNginxDomainCert(c *gin.Context) {
	var reqId request.GetById
	_ = c.ShouldBindJSON(&reqId)

	if err := service.DeleteNginxDomainCert(reqId.Id); err != nil {
		fmt.Printf("删除失败: %v", err.Error())
		response.FailWithMessage("删除失败" + err.Error(), c)

	} else {
		response.OkWithMessage("删除成功", c)
	}
}

// @Tags NginxDomainCert
// @Summary 用id查询NginxDomainCert
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.NginxDomainCert true "用id查询NginxDomainCert"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /nginx/Cert/findNginxDomainCert [get]
func GetNginxDomainCertById(c *gin.Context) {
	var reqId request.GetById
	_ = c.ShouldBind(&reqId)
	if err, reCert := service.GetNginxDomainCertById(reqId.Id); err != nil {
		global.GVA_LOG.Error("查询失败!", zap.Any("err", err))
		response.FailWithMessage("查询失败", c)
	} else {
		response.OkWithData(gin.H{"reCert": reCert}, c)
	}
}

// @Tags NginxDomainCert
// @Summary 分页获取NginxDomainCert列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.NginxDomainCertSearch true "分页获取NginxDomainCert列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /nginx/Cert/getNginxDomainCertList [get]
func GetNginxDomainCertList(c *gin.Context) {
	var pageInfo request.NginxDomainCertSearch
	_ = c.ShouldBind(&pageInfo)
	if err, list, total := service.GetNginxDomainCertInfoList(pageInfo); err != nil {
		global.GVA_LOG.Error("获取失败", zap.Any("err", err))
		response.FailWithMessage("获取失败", c)
	} else {
		response.OkWithDetailed(response.PageResult{
			List:     list,
			Total:    total,
			Page:     pageInfo.Page,
			PageSize: pageInfo.PageSize,
		}, "获取成功", c)
	}
}


