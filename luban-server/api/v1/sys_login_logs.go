package v1

import (
	"fmt"
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
	"gin-luban-server/model/response"
	"gin-luban-server/service"
	"gin-luban-server/utils"
	"github.com/gin-gonic/gin"
	"github.com/mssola/user_agent"
	"go.uber.org/zap"
)

// @Tags SysLoginLogs
// @Summary 分页获取用户登陆信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.PageInfo true "页码, 每页大小"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /loginLogs/getLoginLogsList [get]
func GetLoginLogsList(c *gin.Context) {
	var pageInfo request.SysLoginLogsSearch
	_ = c.ShouldBind(&pageInfo)
	if code, msg := utils.Validate(&pageInfo); code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg, c)
		return
	}
	fmt.Println("aaa", pageInfo.Username)
	if err, list, total := service.GetSysLoginLogsInfoList(pageInfo); err != nil {
		global.GVA_LOG.Error("获取失败!", zap.Any("err", err))
		response.FailWithMessage("获取失败"+err.Error(), c)
	} else {
		response.OkWithDetailed(response.PageResult{
			List:     list,
			Total:    total,
			Page:     pageInfo.Page,
			PageSize: pageInfo.PageSize,
		}, "获取成功", c)
	}
}

// @Tags SysLoginLogs
// @Summary 删除SysLoginLogs
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.SysLoginLogs true "SysLoginLogs模型"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /loginlogs/DeleteLoginLogs  [delete]
func DeleteLoginLogs(c *gin.Context) {
	var loginlogs model.SysLoginLogs
	_ = c.ShouldBind(&loginlogs)
	if err := service.DeleteSysLoginLogs(loginlogs); err != nil {
		global.GVA_LOG.Error("删除失败!", zap.Any("err", err))
		response.FailWithMessage("删除失败！", c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}

// @Tags SysLoginLogs
// @Summary 批量删除SysLoginLogs
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除SysLoginLogs"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"批量删除成功"}"
// @Router /loginlogs/DeleteLoginLogsByIds [delete]
func DeleteLoginLogsByIds(c *gin.Context) {
	var IDS request.IdsReq
	_ = c.ShouldBindJSON(&IDS)
	if err := service.DeleteSysLoginLogsByIds(IDS); err != nil {
		global.GVA_LOG.Error("批量删除失败!", zap.Any("err", err))
		response.FailWithMessage("批量删除失败", c)
	} else {
		response.OkWithMessage("批量删除成功", c)
	}
}

// @Tags SysLoginLogs
// @Summary 创建SysLoginLogs
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.SysLoginLogs true "创建SysLoginLogs"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /loginlogs/CreateLoginLogs [post]
func CreateLoginLogs(c *gin.Context, status *bool, msg string, username string, nickName string) {
	var loginLogs model.SysLoginLogs
	ua := user_agent.New(c.Request.UserAgent())
	loginLogs.Ipaddr = c.ClientIP()
	loginLogs.Username = username
	loginLogs.NickName = nickName
	location := utils.GetLocation(c.ClientIP())
	loginLogs.LoginLocation = location
	loginLogs.Status = status
	loginLogs.Remark = c.Request.UserAgent()
	browserName, browserVersion := ua.Browser()
	loginLogs.Browser = browserName + " " + browserVersion
	loginLogs.Os = ua.OS()
	loginLogs.Msg = msg
	loginLogs.Platform = ua.Platform()
	if err := service.CreateSysLoginLogs(loginLogs); err != nil {
		global.GVA_LOG.Error("创建失败!", zap.Any("err", err))
	}
}

// @Tags SysLoginLogs
// @Summary 根据Id查询SysLoginLogs
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param Id body model.SysLoginLogs true "SysLoginLogs模型"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /loginlogs/GetLoginLogsById [get]

func GetLoginLogsById(c *gin.Context) {
	var idInfo request.GetById
	_ = c.ShouldBind(&idInfo)
	if err := utils.Verify(idInfo, utils.IdVerify); err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err, results := service.GetLoginLogsById(idInfo.Id); err != nil {
		global.GVA_LOG.Error("获取数据失败!", zap.Any("err", err))
		response.FailWithDetailed(response.SysLoginLogsResponse{LoginLogs: results}, "获取数据失败!", c)
	} else {
		response.OkWithDetailed(response.SysLoginLogsResponse{LoginLogs: results}, "获取数据成功!", c)
	}
}
