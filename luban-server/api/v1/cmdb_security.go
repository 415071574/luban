package v1

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
	"gin-luban-server/model/response"
	"gin-luban-server/service"
	"gin-luban-server/utils"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"

)

// @Tags CmdbServer
// @Summary 删除CmdbServer
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.CmdbServer true "删除CmdbServer"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/server/deleteServer  [DELETE]

func DeleteSecurity(c *gin.Context){
	var cmdbSecurity model.CMDBSecurity
	_ = c.ShouldBindJSON(&cmdbSecurity)
	if err := service.DeleteCmdbSecurity(cmdbSecurity); err != nil {
		global.GVA_LOG.Error("删除失败!", zap.Any("err", err))
		response.FailWithMessage("删除失败", c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}

// @Tags CmdbServer
// @Summary 修改mdbServer
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.CmdbServer true "修改CmdbServer"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/server/updateServer  [PUT]
func UpdateSecurity(c *gin.Context) {
	var cmdbSecurity model.CMDBSecurity
	_ = c.ShouldBindJSON(&cmdbSecurity)
	if err := service.UpdateCmdbSecurity(cmdbSecurity); err != nil {
		global.GVA_LOG.Error("更新失败!", zap.Any("err", err))
		response.FailWithMessage("更新失败", c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}

// @Tags CmdbServer
// @Summary 创建CmdbServer
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.CmdbServer true "创建CmdbServer"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/server/createServer  [POST]

func CreateSecurity(c *gin.Context) {
	var cmdbSecuritys model.CMDBSecurity
	_ = c.ShouldBindJSON(&cmdbSecuritys)
	if err := service.CreateCmdbSecuritys(cmdbSecuritys); err != nil {
		global.GVA_LOG.Error("创建失败!", zap.Any("err", err))
		response.FailWithMessage("创建失败", c)
	} else {
		response.OkWithMessage("创建成功", c)
	}
}

// @Tags CmdbSecurity
// @Summary 获取CmdbServer
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.CmdbServer true "获取CmdbSecurity"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/server/getsecurylist  [GET]
// 获取CMDB项目 c *gin.Context

func GetSecurityList(c *gin.Context) {
	var pageInfo request.CMDBSecuritySearch
	_ = c.ShouldBind(&pageInfo)
	if err := utils.Verify(pageInfo.PageInfo, utils.PageInfoVerify); err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err, list, total := service.GetCMDBSecurityInfoList(pageInfo.CMDBSecurity, pageInfo.PageInfo, pageInfo.Desc); err != nil {
		global.GVA_LOG.Error("获取失败!", zap.Any("err", err))
		response.FailWithMessage("获取失败", c)
	} else {
		response.OkWithDetailed(response.PageResult{
			List:     list,
			Total:    total,
			Page:     pageInfo.Page,
			PageSize: pageInfo.PageSize,
		}, "获取成功", c)
	}
}