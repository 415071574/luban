package router

import (
	"gin-luban-server/api/v1"
	"github.com/gin-gonic/gin"
)

func InitAnsibleHostInitRouter(Router *gin.RouterGroup) {
	AnsibleHostInitRouter := Router.Group("host")
	{
		AnsibleHostInitRouter.GET("initWs",v1.AnsibleHostInitWs)  //执行host

	}
}

