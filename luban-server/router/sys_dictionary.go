package router

import (
	"gin-luban-server/api/v1"
	"gin-luban-server/middleware"
	"github.com/gin-gonic/gin"
)

func InitSysDictionaryRouter(Router *gin.RouterGroup) {
	SysDictionaryRouter := Router.Group("dict").Use(middleware.OperationRecord())
	{
		SysDictionaryRouter.POST("createDictionary", v1.CreateDictionary)   // 新建SysDictionary
		SysDictionaryRouter.DELETE("deleteDictionary", v1.DeleteDictionary) // 删除SysDictionary
		SysDictionaryRouter.PUT("updateDictionary", v1.UpdateDictionary)    // 更新SysDictionary
		SysDictionaryRouter.GET("findDictionaryById", v1.FindDictionaryById)    // 根据ID获取SysDictionary
		SysDictionaryRouter.GET("getDictionary", v1.GetDictionary)          // 获取SysDictionary
		SysDictionaryRouter.GET("getDictionaryList", v1.GetDictionaryList)  // 获取SysDictionary列表
	}
}
