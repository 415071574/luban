package router

import (
	v1 "gin-luban-server/api/v1"
	"gin-luban-server/middleware"
	"github.com/gin-gonic/gin"
)

func InitJumpServerLogs(Router *gin.RouterGroup) {
	JumpServerLogsRouter := Router.Group("logs").Use(middleware.OperationRecord())
	{
		JumpServerLogsRouter.PUT("updateJumpServerSshLogs", v1.UpdateJumpServerSshLogs) //新增
		JumpServerLogsRouter.GET("getJumpServerSshLogsList",v1.GetJumpServerSshLogsList) //查询接口
		JumpServerLogsRouter.DELETE("deleteJumpServerSshLogs",v1.DeleteJumpServerSshLogs)  //删除
		JumpServerLogsRouter.DELETE("deleteJumpServerSshLogsByIds",v1.DeleteJumpServerSshLogsByIds)  //删除
	}
}
