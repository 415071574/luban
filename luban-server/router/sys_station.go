package router

import (
	v1 "gin-luban-server/api/v1"
	"gin-luban-server/middleware"
	"github.com/gin-gonic/gin"
)

func InitStationRouter(Router *gin.RouterGroup) {
	SysStationRouter := Router.Group("station").Use(middleware.OperationRecord())
	{
		SysStationRouter.POST("createStation", v1.CreateStation)   // 添加岗位
		SysStationRouter.PUT("updateStation", v1.UpdateStation)   // 更新岗位
		SysStationRouter.DELETE("deleteStation", v1.DeleteStation) // 删除岗位
		SysStationRouter.GET("getStationList", v1.GetStationList)  // 获取岗位列表
		SysStationRouter.GET("getStationById", v1.GetStationById)  // 根据Id获取岗位列表
		SysStationRouter.GET("getStationByCode", v1.GetStationByCode)  // 根据公司和部门code获取岗位列表
	}
}
