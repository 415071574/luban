package router

import (
	v1 "gin-luban-server/api/v1"
	"gin-luban-server/middleware"
	"github.com/gin-gonic/gin"
)

func InitDepartmentRouter(Router *gin.RouterGroup) {
	SysDepartmentRouter := Router.Group("department").Use(middleware.OperationRecord())
	{
		SysDepartmentRouter.POST("createDepartment", v1.CreateDepartment)   // 添加部门
		SysDepartmentRouter.PUT("updateDepartment", v1.UpdateDepartment)   // 更新部门
		SysDepartmentRouter.DELETE("deleteDepartment", v1.DeleteDepartment) // 删除部门
		SysDepartmentRouter.POST("getDepartmentList", v1.GetDepartmentList)  // 获取部门列表
		SysDepartmentRouter.POST("getDepartmentById", v1.GetDepartmentById)  // 通过Id获取部门信息
		SysDepartmentRouter.GET("getDepartmentByCode", v1.GetDepartmentByCode)  // 通过公司code获取部门信息
	}
}
