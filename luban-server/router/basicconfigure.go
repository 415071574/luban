package router

import (
	v1 "gin-luban-server/api/v1"
	"gin-luban-server/middleware"
	"github.com/gin-gonic/gin"
)

func InitBasicConfigureRouter(Router *gin.RouterGroup) {
	SysBasicConfigureRouter := Router.Group("basic").Use(middleware.OperationRecord())
	{
		SysBasicConfigureRouter.POST("createBasicConfigure", v1.CreateBasicConfigure)   // 添加基础配置
		SysBasicConfigureRouter.PUT("updateBasicConfigure", v1.UpdateBasicConfigure)   // 更新基础配置
		SysBasicConfigureRouter.DELETE("deleteBasicConfigure", v1.DeleteBasicConfigure) // 删除基础配置
		SysBasicConfigureRouter.GET("getBasicConfigureList", v1.GetBasicConfigureList) // 获取基础配置列表
	}
}

