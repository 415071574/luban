import service from '@/utils/request'
// @Tags api
// @Summary 分页获取岗位列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body modelInterface.PageInfo true "分页获取部门列表"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /department/getDepartmentList [post]
// {
//  page     int
//	pageSize int
// }
export const getStationList = (params) => {
    return service({
        url: "/station/getStationList",
        method: 'get',
        params
    })
}
// @Tags Api
// @Summary 新增岗位
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.createStation Params true "新增岗位"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"添加成功"}"
// @Router /station/createStation[post]
export const createStation = (data) => {
    return service({
        url: "/station/createStation",
        method: 'post',
        data
    })
}

// @Tags SysUser
// @Summary 删除部门
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.SetUserAuth true "删除部门"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"修改成功"}"
// @Router /department/deleteDepartment [delete]
export const deleteStation = (data) => {
    return service({
        url: "/station/deleteStation",
        method: 'delete',
        data: data
    })
}

// @Tags Api
// @Summary 根据Id查询岗位信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.getStationById  true "根据Id查询岗位信息"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /station/getStationById[get]

export const getStationById = (data) => {
    return service({
        url: "/station/getStationById",
        method: 'post',
        data: data
    })
}
// @Tags Api
// @Summary 更新岗位信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.updateStation  true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"添加成功"}"
// @Router /station/updateStation[put]
export const updateStation = (data) => {
    return service({
        url: "/station/updateStation",
        method: 'put',
        data: data
    })
}

// @Tags Api
// @Summary 查询岗位信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.getDepartment  true "查询岗位信息"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /station/getStation[get]
export const getStationByCode = (params) => {
    return service({
        url: "/station/getStationByCode",
        method: 'get',
        params
    })
}
