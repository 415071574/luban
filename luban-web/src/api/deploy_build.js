import service from '@/utils/request'
// @Tags api
// @Summary 分页获取应用部署列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.DeployBuild PageInfo true "分页获取用户列表"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /deploy/build/getDeployBuildList [Get]
// {
//  page     int
//	pageSize int
// }
export const getDeployBuildList = (data) => {
    return service({
        url: "/deploy/build/getDeployBuildList",
        method: 'post',
        data
    })
}


// @Tags Api
// @Summary 获取应用分支列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.UpdateDeployApplyVirtualParams true "更新api"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /deploy/build/getAppGitRepoInfo [get]
export const getAppGitRepoInfo = (params) => {
    return service({
        url: "/deploy/build/getAppGitRepoInfo",
        method: 'get',
        params
    })
}

// @Tags Api
// @Summary 获取应用部署服务器列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.ApplyVirtualParams true "更新api"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /deploy/build/getAppVirtualList [get]
export const getAppVirtualList = (params) => {
    return service({
        url: "/deploy/build/getAppVirtualList",
        method: 'get',
        params
    })
}
