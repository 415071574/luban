import service from '@/utils/request'
import { handleFileError } from '@/utils/download'
// @Tags api
// @Summary 分页获取数据库列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.CMDBDatabase.PageInfo true "分页获取用户列表"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/database/getDatabaseList [Get]
// {
//  page     int
//	pageSize int
// }
export const getDatabaseList = (params) => {
    return service({
        url: "/cmdb/database/getDatabaseList",
        method: 'get',
        params
    })
}

// @Tags Api
// @Summary 更新数据库信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.UpdateDatabaseParams true "更新api"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /cmdb/database/updateDatabase [post]
export const updateDatabase = (data) => {
    return service({
        url: "/cmdb/database/updateDatabase",
        method: 'put',
        data
    })
}


// @Tags Api
// @Summary 新增数据信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.CreateDatabaseParams true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"添加成功"}"
// @Router /cmdb/database/CreateDatabase [post]

export const createDatabase = (data) => {
    return service({
        url: "/cmdb/database/createDatabase",
        method: 'post',
        data
    })
}


// @Tags Api
// @Summary 删除数据库信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.deleteDatabaseParams true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /cmdb/database//deleteDatabase [post]
export const deleteDatabase = (data) => {
    return service({
        url: "/cmdb/database/deleteDatabase",
        method: 'delete',
        data
    })
}

// @Tags Api
// @Summary 根据Id查询数据库信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.CreateDatabaseParams true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"添加成功"}"
// @Router /cmdb/database/getDatabaseById[get]

export const getDatabaseById = (params) => {
    return service({
        url: "/cmdb/database/getDatabaseById",
        method: 'get',
        params
    })
}


// @Tags Api
// @Summary 导出数据库信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.exportExcel true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"添加成功"}"
// @Router /cmdb/database/exportExcel[get]

export const exportExcel = (params) => {
    return service({
        url: "/cmdb/database/exportExcel",
        method: 'get',
        params,
        responseType: 'blob'
    }).then(res => {
        handleFileError(res, params.fileName)
    })
}
