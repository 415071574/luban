let config = {};

config.wsBase = `${window.location.protocol === "http:" ? "ws://" : "wss://"}${window.location.host}/jump/server/webSshWs`;

export default config;
