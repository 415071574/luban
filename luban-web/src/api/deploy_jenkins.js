import service from '@/utils/request'
// @Tags Api
// @Summary 更新项目应用jenkins JOB信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.GetById true "更新api"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /deploy/jenkins/updateDeployAppJenkins [put]
export const updateDeployAppJenkins = (data) => {
    return service({
        url: "/deploy/jenkins/updateDeployAppJenkins",
        method: 'put',
        data
    })
}


// @Tags Api
// @Summary 新增项目应用jenkins JOB信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.GetById true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"添加成功"}"
// @Router /deploy/jenkins/createDeployAppJenkins [post]

export const createDeployAppJenkins = (data) => {
    return service({
        url: "/deploy/jenkins/createDeployAppJenkins",
        method: 'post',
        data
    })
}



// @Tags Api
// @Summary 构建项目应用jenkins JOB
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.JenkinsJobBuildParams true "构建"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /deploy/jenkins/getJenkinsBuildJob[post]
export const getJenkinsBuildJob = (data) => {
    return service({
        url: "/deploy/jenkins/getJenkinsBuildJob",
        method: 'post',
        data
    })
}




// 启动部署
export const startJenkinsJobBuild = (data) => {
    return service({
        url: "/deploy/jenkins/startJenkinsJobBuild",
        method: 'post',
        data
    })
}

// 启动回滚
export const rollbackJenkinsJobBuild = (data) => {
    return service({
        url: "/deploy/jenkins/rollbackJenkinsJobBuild",
        method: 'post',
        data
    })
}


// 获取日志
export const getJenkinsBuildJobLogs = (data) => {
    return service({
        url: "/deploy/jenkins/getJenkinsBuildJobLogs",
        method: 'post',
        data
    })
}